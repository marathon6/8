import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {
    val job = launch {
        try {
            repeat(3) { i ->
                delay(1000)
                println("I'm sleeping $i ...\n")
            }
        } finally {
            delay(2000)
            println("I'm running finally\n")
        }
    }
    delay(4000)
    println("main: I'm tired of waiting!\n")
    delay(2000)
    println("main: Now I can quit.")
}
