import kotlinx.coroutines.*

fun main() = runBlocking {
    launch {
        delay(1000)
        println("World")
    }
    delay(2000)
    println("Hello, ")
}

// Пока искал инфу о корутинах, нашел еще такой вариант исполнения:

/*suspend fun message(text: String, delay: Long){
    delay(delay)
    println(text)
}

fun main() = runBlocking {
    joinAll(
        async { message("World", 1000) },
        async { message("Hello, ", 2000) }
    )
}*/
