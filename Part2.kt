import kotlinx.coroutines.*

suspend fun num1(): Int{
    delay(1000)
    return 1
}

suspend fun num2(): Int{
    delay(2000)
    return 2
}

fun main() = runBlocking {
    var startTime = System.currentTimeMillis()

    val first = num1()
    val second = num2()

    var endTime = System.currentTimeMillis()

    println("Sum equals ${first + second} and ${endTime - startTime} milliseconds were spent ")

    startTime = System.currentTimeMillis()

    val firstCase2 =  async { num1() }
    val secondCase2 = async { num2() }

    val resultCase2 = runBlocking { firstCase2.await() + secondCase2.await() }

    endTime = System.currentTimeMillis()

    println("Sum equals $resultCase2 and ${endTime - startTime} milliseconds were spent ")
    // Честно говоря, не уверен что правильно понял суть задания, но в случае асинхронного вызова, обе функции
    // вызываются вместе, когда в первом случае последовательно, дожидаясь окончания первой
}